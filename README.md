# About this repository #
It contains an assignment project where I have to create a simple react-native project.  
I try to make the `readme.md` of this repository dinamic upon the branches. Means, individual branch shows that's feature behaviours.  

# Overview of this project #
This is a react-native application where I have to bulid the project within three stages.  

### In Part 1 ###
I have to create an app with TabNavigation with 4 screens. The first 3 screens will load different texts and images. The image will cover the entire screen.  
There will be a text at the top of the screen that says the name of the screen and a footer. There will be use of ONE component for the first 3 screens.

### In Part 2 ###
For the last screen it will show the current time in your app. The time should update live as the device time changes.

### In Part 3 ###
it will store a note from the app. Even if the user closes the app the data should persist. Create a simple database that stores a “Note time” and “Note”.  
If there is no existing note it should say Last note created at: NEVER!!! And show (Tap to add note) inside the Edit box.  
When user taps on the Note section it should open up a Edit Note screen using stack navigator. In the Edit Note screen user will be able to edit the notes and hit save.  
When user save the Note, the database will get updated.

Once configured, users/developers need to define their custom event handlers, by writing some Python love.  

# Install Dependency #
To run this project we need:  
#### 1. NodeJS v10.19.0(test on it) or higher.  
#### 2. Npm v7.5.2(test on it) or higher.  

### If we want to run the project through Android Studio then we need:  
#### 1. JRE v11 (test on it) or higher.  
#### 2. JDK v15.0.2 (test on it) or higher.  
#### 3. Android Studio.  
The linekd documents show how to install these Dependency for different OSs.
[NodeJS](https://nodejs.dev/learn/how-to-install-nodejs) | 
[NPM](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) | 
[JRE](https://ubuntu.com/tutorials/install-jre#2-installing-openjre)  |
[JDK](https://www3.ntu.edu.sg/home/ehchua/programming/howto/JDK_Howto.html)  |
[Android Studio](https://developer.android.com/studio/install)  |  

## Install Dependencies for linux
This is only for linux OS as I think the installation of those are pretty much same for windows and mac, which can be found in those documents.  
Now, to install those dependencies:  

- Install node.js
```
sudo apt update
sudo apt install nodejs
```

- Install "Node.js package manager", npm.
```
sudo apt install npm
```

### For working on Emulator

- Install Java(JRE). Say the version is "openjdk-11-jre-headless"
```
sudo apt install openjdk-11-jre-headless
```

- Download deb package of JDK & Install using dpkg. Say the file is "dpkg -i jdk-15.0.2_linux-x64_bin.deb"
```
sudo dpkg -i jdk-15.0.2_linux-x64_bin.deb
```

- Download & Extract the Android Studio and rename the extracted file as 'android-studio'.  
- Go to " android-studio/bin " folder and run sh file
```
./studio.sh
```

- Select Custom and then next ....
- Select All the necessary elements in "SDK Component Setup" and next .... and finish
```
Android SDK
Android SDK Platform
Android Virtual Device
```

- To create desktop shortcut  
Click on "Configure" button and select "Create Desktop Entry". Then, check the box of "Create the entry for all users" and hit OK

#### To setup SDK Manager

- Open Android Studio using the desktop shortcut or using sh file as
```
./studio.sh
```

- Click on "Configure" button and select "SDK Manager"
- Select the "SDK Platforms" tab from within the SDK Manager, then check the box next to "Show Package Details" in the bottom right corner.  
Look for and expand the Latest Android version, then make sure the following items are checked:
```
Android SDK Platform
Intel x86 Atom_64 System Image
Google APIs Intel x86 Atom_64 System Image
```

- Next, select the "SDK Tools" tab and make sure all these are checked (if exists)
```
Android SDK Build-Tools
Android Emulator
Android SDK Platform-Tools
Intel x86 Emulator Accelerator
```

- Finally, click "Apply" to download, then accept and install the Android SDK and related build tools.

### Configure the ANDROID_HOME environment variable.

- Open Android Studio using sh file as
```
./studio.sh
```
- Click on "Configure" button and select "SDK Manager"
- Copy the path of "Android SDK Location:"
- Go to the home/user and open .bashrc file in root mode using a text editor
```
cd ~
sudo gedit .bashrc
```
- Paste these lines at the end of the file and replace the value of "export ANDROID_HOME=" and "export ANDROID_SDK=" with the copied path
```
export ANDROID_HOME=/home/mz/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

export ANDROID_SDK=/home/mz/Android/Sdk
export PATH=$ANDROID_SDK/emulator:$ANDROID_SDK/tools:$PATH
```
Add the following lines to your HOME/.bash_profile or HOME/.bashrc (if you are using zsh then also add in ~/.zprofile or ~/.zshrc) config file.

- Save & Load the file and check the file configure is correct or not by type
```
cd ~
source .bashrc
adb
```
Note: Please make sure you use the correct Android SDK path.  
You can find the actual location of the SDK in the Android Studio "Preferences" dialog, under Appearance & Behavior → System Settings → Android SDK.

- After install Android Studio go to the "platform-tools" folder
```
cd ~/Android/Sdk/platform-tools
```

- Copy adb from Android SDK directory to usr/bin directory:
```
sudo cp adb /usr/bin
```

## Run the project
The above processes are needed at only initalization and if they are done already then do only the bellow steps to run this project.  

- To run in emulator first go to the emulaotor directory of Android Studion
```
cd ~/Android/Sdk/emulator/
```

- Check if you have any emulator device or not and what is the name of the device
```
emulator -list-avds
```
Note: If there is no device then you have to create at least one. Follow [this link](https://developer.android.com/studio/run/managing-avds) to create an emulator device.  

- Run the device by the command where re place the "< device name >" with the name of the device
```
./emulator -avd < device name >
```

- Check dependent package libraries are available or not and install if necessary. To do so, open a new terminal into the project directory and type
```
npm install
```

- To run the project, type
```
npx react-native start
```

- To run the project in the emulator, open a new terminal into the project directory and type
```
npx react-native run-android
```

